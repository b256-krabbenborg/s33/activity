fetch("https://jsonplaceholder.typicode.com/todos")
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  });

fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(data => {
    console.log(`Title: ${data.title}, Status: ${data.completed ? "completed" : "not completed"}`);
  });

fetch("https://jsonplaceholder.typicode.com/todos", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "New Task",
    description: "A new task description",
    completed: false,
    dateCompleted: null,
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    id: 1,
    title: "Updated Task",
    description: "Updated task description",
    completed: true,
    dateCompleted: "2023-04-05",
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    completed: true,
    dateCompleted: "2023-04-05"
  })
})
  .then(response => response.json())
  .then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
});
